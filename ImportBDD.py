import csv
import sqlite3
import logging
import os
import atexit

def ouverturecsv(fichier):

    if(os.path.exists(fichier)):
        if(os.path.isfile(fichier)):
            with open(fichier) as csvfile:
                reader = csv.reader(csvfile, delimiter = ';')
                for row in reader:
                    Rows.append(row)
                logging.debug("Le fichier .csv est récupéré")
        else:
            logging.warning("Pas le bon délimiteur")
    else:
        logging.warning("nom de fichier incorrect")
    return(Rows)
          ## lecture du fichier csv puis retourne le nom des colonnes dans une liste

def verif_sql(fichier):
    if(os.path.exists(fichier)):
        logging.info('BDD existante')
    else :
        logging.warning("ATTENTION : BDD inexistante")
    connection = sqlite3.connect(fichier)
    connection.commit()
    connection.close()
## vérification de l'existance de la base de donnée, retour du résultat dans les logs

def creationbdd(bdd, Rows):
    connection = sqlite3.connect(bdd)
    cursor = connection.cursor()
    logging.debug("Connecté pour création")

    commande = open("cmd.txt", "r")
    cursor.execute(commande.read())
    ## création de la BDD à partir de commandes sql écrites dans un fichier texte

def connectmodif(fichier,Rows):

    connection = sqlite3.connect(bdd)
    cursor = connection.cursor()
    logging.debug("Connecté pour modif")

    for row in Rows :
        
        cursor.execute("SELECT * FROM autobdd WHERE immatriculation=?", (row[2],))
        if (cursor.fetchall() == []) :
            cursor.execute("INSERT OR IGNORE INTO autobdd VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18]))
        else :
            cursor.execute("UPDATE autobdd set adresse_titulaire=?, nom=?, prenom=?, immatriculation=?, date_immatriculation=?, vin=?, marque=?, denomination_commerciale=?, couleur=?, carrosserie=?, categorie=?, cylindree=?, energie=?, places=?, poids=?, puissance=?, type=?, variante=?, version=?", (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18]))
        connection.commit()
        
##connexion à la base puis modification. On créé le nouveau champ si il n'existe pas ou on le modifie


def ferme_connection(connection, cursor):
    if connection :
        cursor.close()
        connection.close()
        logging.info("La connection est fermée")
##déconnexion de la base de donnée        

def init():
    logging.basicConfig(filename = 'example.log', level = logging.DEBUG)
    logging.info("Connection initialisée")
    atexit.register(ferme_connection, sqlite3.connect(bdd), sqlite3.connect(bdd).cursor())


fichier = "newauto.csv"
bdd = "autoBDD.sqlite3"
Rows = []
init()

Rows = ouverturecsv(fichier)
verif_sql(bdd)
creationbdd(bdd, Rows)
connectmodif(bdd, Rows)
##appel des différentes fonctions
