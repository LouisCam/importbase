from ImportBDD import *
import unittest



class TestImportBDD(unittest.TestCase):

    def testexistbase(self):
        self.assertEqual(os.path.exists("autoBDD.sqlite3"), True)
        self.assertEqual(os.path.exists("toto.sqlite3"), False)

    def testexisttable(self):
        commande = ''
        connection=sqlite3.connect('autoBDD.sqlite3')
        self.tablename='test'
        cursor= connection.cursor()
        commande = open("testcreation.txt", 'r')
        cursor.execute(commande.read())

        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        self.assertEqual(cursor.fetchall(),[('autobdd',), ('test',)])
        commande.close()
        connection.commit()
        cursor.close()
        connection.close()

    def testinsert(self):
        connection = sqlite3.connect("autoBDD.sqlite3")
        cursor = connection.cursor()
        cursor.execute("INSERT INTO test VALUES ('1', 'LaD')")
        cursor.execute("SELECT * FROM test;")
        self.assertEqual(cursor.fetchone(), (1, 'LaD'))
        cursor.execute("DROP TABLE test")
        connection.commit()
        cursor.close()
        connection.close()